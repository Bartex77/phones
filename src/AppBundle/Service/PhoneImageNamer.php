<?php

namespace AppBundle\Service;

use Vich\UploaderBundle\Naming\NamerInterface;
use Vich\UploaderBundle\Mapping\PropertyMapping;

/**
 * Class PhoneImageNamer
 * @package AppBundle\Service
 *
 * Custom namer for picture files
 */
class PhoneImageNamer implements NamerInterface
{
    /**
     * Creates a name for the file being uploaded.
     *
     * @param object          $object  The object the upload is attached to.
     * @param PropertyMapping $mapping The mapping to use to manipulate the given object.
     *
     * @return string The file name.
     */
    public function name($object, PropertyMapping $mapping)
    {
        $returnOrderId = $object->getPhoneOrder()->getReturnOrderId();
        $extension = '.';

        return $returnOrderId . $extension;
    }
}
