<?php

namespace AppBundle\Model;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\PhoneOrder;

class PhoneOrderModel
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * PhoneOrderModel constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createPhoneOrder(PhoneOrder $phoneOrder)
    {
        $this->entityManager->persist($phoneOrder);
        $this->entityManager->flush();
    }
}
