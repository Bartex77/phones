<?php

namespace AppBundle\Form\Handler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;
use AppBundle\Entity\PhoneOrder;
use AppBundle\Model\PhoneOrderModel;

class CreatePhoneOrderFormHandler
{
    /**
     * @var PhoneOrderModel
     */
    private $phoneOrderModel;

    /**
     * CreatePhoneOrderFormHandler constructor.
     * @param PhoneOrderModel $phoneOrderModel
     */
    public function __construct(PhoneOrderModel $phoneOrderModel)
    {
        $this->phoneOrderModel = $phoneOrderModel;
    }

    public function handle(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var PhoneOrder $validPhoneOrder */
            $validPhoneOrder = $form->getData();
            $this->phoneOrderModel->createPhoneOrder($validPhoneOrder);

            return true;
        }

        return false;
    }
}
