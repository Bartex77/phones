<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Entity\PhoneOrder;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use AppBundle\Form\Type\ImeiType;
use AppBundle\Form\Type\PhoneImageType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PhoneOrderType extends AbstractType
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('returnOrderId')
            ->add('orderName')
            ->add('ean')
            ->add(
                'phoneCondition',
                ChoiceType::class,
                [
                    'choices' => $this->container->getParameter('phone_condition_labels')
                ])
            ->add(
                'orderStatus',
                ChoiceType::class,
                [
                    'choices' => $this->container->getParameter('order_status_labels')
                ])
            ->add('description')
            ->add('saleOrderId')
            ->add('imeis', CollectionType::class, [
                'entry_type'   => ImeiType::class,
                'allow_add'    => true,
                'by_reference' => false,
            ])
            ->add('save',SubmitType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PhoneOrder::class,
        ]);
        $resolver->setRequired('service_container');
    }
}
