<?php

namespace AppBundle\DataProvider;

use AppBundle\Repository\PhoneOrderRepository;

class PhoneOrderDataProvider
{
    /**
     * @var PhoneOrderRepository
     */
    private $phoneOrderRepository;

    /**
     * PhoneOrderDataProvider constructor.
     *
     * @param PhoneOrderRepository $phoneOrderRepository
     */
    public function __construct(PhoneOrderRepository $phoneOrderRepository)
    {
        $this->phoneOrderRepository = $phoneOrderRepository;
    }

    public function fetchPhoneOrderList()
    {
        return $this->phoneOrderRepository->findAll();
    }
}
