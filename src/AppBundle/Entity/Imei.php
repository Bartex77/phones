<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\PhoneOrder;

/**
 * Imei
 *
 * @ORM\Table(name="imei")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImeiRepository")
 */
class Imei
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="imei", type="integer", unique=true)
     */
    private $imei;

    /**
     * @ORM\ManyToOne(targetEntity="PhoneOrder", inversedBy="imeis")
     * @ORM\JoinColumn(name="phone_order_id", referencedColumnName="id")
     */
    private $phoneOrder;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imei
     *
     * @param integer $imei
     *
     * @return Imei
     */
    public function setImei($imei)
    {
        $this->imei = $imei;

        return $this;
    }

    /**
     * Get imei
     *
     * @return int
     */
    public function getImei()
    {
        return $this->imei;
    }

    /**
     * @param \AppBundle\Entity\PhoneOrder $phoneOrder
     */
    public function setPhoneOrder(PhoneOrder $phoneOrder)
    {
        $this->phoneOrder = $phoneOrder;
    }

    /**
     * @return PhoneOrder
     */
    public function getPhoneOrder()
    {
        return $this->phoneOrder;
    }
}
