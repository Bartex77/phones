<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PhoneOrder
 *
 * @ORM\Table(name="phone_order")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PhoneOrderRepository")
 */
class PhoneOrder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Imei", mappedBy="phoneOrder", cascade={"persist"})
     */
    private $imeis;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PhoneImage", mappedBy="phoneOrder", cascade={"persist"})
     */
    private $phoneImages;

    /**
     * @var int
     *
     * @ORM\Column(name="returnOrderId", type="integer", unique=true)
     */
    private $returnOrderId;

    /**
     * @var int
     *
     * @ORM\Column(name="ean", type="integer", unique=true)
     */
    private $ean;

    /**
     * @var int
     *
     * @ORM\Column(name="phoneCondition", type="integer")
     */
    private $phoneCondition;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="orderStatus", type="integer")
     */
    private $orderStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="orderDate", type="date")
     */
    private $orderDate;

    /**
     * @var string
     *
     * @ORM\Column(name="orderName", type="string", length=255)
     */
    private $orderName;

    /**
     * @var int
     *
     * @ORM\Column(name="saleOrderId", type="integer", nullable=true)
     */
    private $saleOrderId;

    public function __construct()
    {
        $this->imeis = new ArrayCollection();
        $this->phoneImages = new ArrayCollection();
        $this->orderDate = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection
     */
    public function getImeis()
    {
        return $this->imeis;
    }

    /**
     * @param ArrayCollection $imeis
     */
    public function setImeis(ArrayCollection $imeis)
    {
        $this->imeis = $imeis;
    }

    /**
     * @param Imei $imei
     */
    public function addImei(Imei $imei)
    {
        $this->imeis->add($imei);
        $imei->setPhoneOrder($this);
    }

    /**
     * @param Imei $imei
     */
    public function removeImei(Imei $imei)
    {
        $this->imeis->removeElement($imei);
    }

    /**
     * @return ArrayCollection
     */
    public function getPhoneImages()
    {
        return $this->phoneImages;
    }

    /**
     * @param ArrayCollection $phoneImages
     */
    public function setPhoneImages(ArrayCollection $phoneImages)
    {
        $this->phoneImages = $phoneImages;
    }

    /**
     * @param PhoneImage $phoneImage
     */
    public function addPhoneImage(PhoneImage $phoneImage)
    {
        $this->phoneImages->add($phoneImage);
    }

    /**
     * @param PhoneImage $phoneImage
     */
    public function removePhoneImage(PhoneImage $phoneImage)
    {
        $this->phoneImages->removeElement($phoneImage);
    }

    /**
     * Set returnOrderId
     *
     * @param integer $returnOrderId
     *
     * @return PhoneOrder
     */
    public function setReturnOrderId($returnOrderId)
    {
        $this->returnOrderId = $returnOrderId;

        return $this;
    }

    /**
     * Get returnOrderId
     *
     * @return int
     */
    public function getReturnOrderId()
    {
        return $this->returnOrderId;
    }

    /**
     * Set ean
     *
     * @param integer $ean
     *
     * @return PhoneOrder
     */
    public function setEan($ean)
    {
        $this->ean = $ean;

        return $this;
    }

    /**
     * Get ean
     *
     * @return int
     */
    public function getEan()
    {
        return $this->ean;
    }

    /**
     * Set phoneCondition
     *
     * @param integer $phoneCondition
     *
     * @return PhoneOrder
     */
    public function setPhoneCondition($phoneCondition)
    {
        $this->phoneCondition = $phoneCondition;

        return $this;
    }

    /**
     * Get phoneCondition
     *
     * @return int
     */
    public function getPhoneCondition()
    {
        return $this->phoneCondition;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return PhoneOrder
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set orderStatus
     *
     * @param integer $orderStatus
     *
     * @return PhoneOrder
     */
    public function setOrderStatus($orderStatus)
    {
        $this->orderStatus = $orderStatus;

        return $this;
    }

    /**
     * Get orderStatus
     *
     * @return int
     */
    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    /**
     * Set orderDate
     *
     * @param \DateTime $orderDate
     *
     * @return PhoneOrder
     */
    public function setOrderDate($orderDate)
    {
        $this->orderDate = $orderDate;

        return $this;
    }

    /**
     * Get orderDate
     *
     * @return \DateTime
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * Set orderName
     *
     * @param string $orderName
     *
     * @return PhoneOrder
     */
    public function setOrderName($orderName)
    {
        $this->orderName = $orderName;

        return $this;
    }

    /**
     * Get orderName
     *
     * @return string
     */
    public function getOrderName()
    {
        return $this->orderName;
    }

    /**
     * Set saleOrderId
     *
     * @param integer $saleOrderId
     *
     * @return PhoneOrder
     */
    public function setSaleOrderId($saleOrderId)
    {
        $this->saleOrderId = $saleOrderId;

        return $this;
    }

    /**
     * Get saleOrderId
     *
     * @return int
     */
    public function getSaleOrderId()
    {
        return $this->saleOrderId;
    }
}

