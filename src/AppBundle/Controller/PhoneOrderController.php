<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\PhoneOrder;
use AppBundle\Form\Type\PhoneOrderType;

class PhoneOrderController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $phoneOrderDataProvider = $this->get('data_provider.phone_order');
        $phoneOrderList = $phoneOrderDataProvider->fetchPhoneOrderList();

        return $this->render('@AppBundle/Resources/views/index.html.twig', [
            'phone_order_list' => $phoneOrderList,
            'phone_condition' => $this->getParameter('phone_condition_labels'),
            'order_status' => $this->getParameter('order_status_labels'),
        ]);
    }

    /**
     * @Route("/new", name="newPhoneOrder")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newPhoneOrderAction(Request $request)
    {
        $phoneOrder = new PhoneOrder();

        $form = $this->createForm(PhoneOrderType::class, $phoneOrder, [
            'service_container' => $this->get('service_container')
        ]);

        $formHandler = $this->get('form_handler.create_phone_order');

        if ($formHandler->handle($form, $request)) {
            return $this->redirectToRoute('homepage');
        }

        return $this->render('@AppBundle/Resources/views/newPhoneOrder.html.twig',[
                'form' => $form->createView()
        ]);
    }
}
