var $imeiCollectionHolder;
var $addImeiLink = $('<a href="#" class="add_imei_link">Add an IMEI</a>');
var $newImeiLinkLi = $('<li></li>').append($addImeiLink);

var $phoneImageCollectionHolder;
var $addPhoneImageLink = $('<a href="#" class="add_imei_link">Add phone image</a>');
var $newPhoneImageLinkLi = $('<li></li>').append($addPhoneImageLink);

jQuery(document).ready(function() {
    $imeiCollectionHolder = $('ul.imeis');
    $imeiCollectionHolder.append($newImeiLinkLi);

    $imeiCollectionHolder.data('index', $imeiCollectionHolder.find(':input').length);

    addForm($imeiCollectionHolder, $newImeiLinkLi);
    $addImeiLink.on('click', function(e) {
        e.preventDefault();
        addForm($imeiCollectionHolder, $newImeiLinkLi);
    });

    $phoneImageCollectionHolder = $('ul.phoneImages');
    $phoneImageCollectionHolder.append($newPhoneImageLinkLi);

    $phoneImageCollectionHolder.data('index', $phoneImageCollectionHolder.find(':input').length);

    addForm($phoneImageCollectionHolder, $newPhoneImageLinkLi);
    $addPhoneImageLink.on('click', function(e) {
        e.preventDefault();
        addForm($phoneImageCollectionHolder, $newPhoneImageLinkLi);
    });
});

function addForm($collectionHolder, $newLinkLi) {
    var prototype = $collectionHolder.data('prototype');
    var index = $collectionHolder.data('index');
    var newForm = prototype.replace(/__name__/g, index);

    $collectionHolder.data('index', index + 1);

    var $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi.before($newFormLi);
}
